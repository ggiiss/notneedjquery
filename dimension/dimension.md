## 坐标

## 尺寸


$(el).offset();

$(el).offsetParent();

$(el).outerHeight();

$(el).outerHeight(true);

$(el).outerWidth();

$(el).outerWidth(true);

$(el).position();

var offset = el.offset();

{
  top: offset.top - document.body.scrollTop,
  left: offset.left - document.body.scrollLeft
}


var rect = el.getBoundingClientRect()

{
  top: rect.top + document.body.scrollTop,
  left: rect.left + document.body.scrollLeft
}

el.offsetParent || el

el.offsetHeight

function outerHeight(el) {
  var height = el.offsetHeight;
  var style = getComputedStyle(el);

  height += parseInt(style.marginTop) + parseInt(style.marginBottom);
  return height;
}

outerHeight(el);

el.offsetWidth

function outerWidth(el) {
  var width = el.offsetWidth;
  var style = getComputedStyle(el);

  width += parseInt(style.marginLeft) + parseInt(style.marginRight);
  return width;
}

outerWidth(el);

el.parentNode


{left: el.offsetLeft, top: el.offsetTop}

//  Position Relative To Viewport
el.getBoundingClientRect()

parent.insertBefore(el, parent.firstChild);