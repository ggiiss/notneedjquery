## DOM操作 Manipulation

$(el).after(htmlString);

$(parent).append(el);

$(el).before(htmlString);

$(el).clone();

$(el).empty();

$(parent).prepend(el);

$('<div>').append($(el).clone()).html();

el.insertAdjacentHTML('afterend', htmlString);

parent.appendChild(el);

el.insertAdjacentHTML('beforebegin', htmlString);