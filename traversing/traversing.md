# DOM遍历 Traversing

功能：父元素、祖先元素、兄弟元素、子元素

实现：遍历函数、工具函数、模版函数

$(el).children();

$(el).parent();

$(el).next();

$.contains(el, child);

$(el).find(selector).length;

$(selector).each(function(i, el){

});


$(selector).filter(filterFn);

$(el).find(selector);

$(el).hasClass(className);

$(el).is($(otherEl));

$(el).is('.my-class');
