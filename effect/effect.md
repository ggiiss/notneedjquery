基本
show([speed,[easing],[fn]])
hide([speed,[easing],[fn]])
toggle([speed],[easing],[fn])
滑动
slideDown([spe],[eas],[fn])
slideUp([speed,[easing],[fn]])
slideToggle([speed],[easing],[fn])
淡入淡出
fadeIn([speed],[eas],[fn])
fadeOut([speed],[eas],[fn])
fadeTo([[spe],opa,[eas],[fn]])
fadeToggle([speed,[eas],[fn]])
自定义
animate(param,[spe],[e],[fn])
stop([cle],[jum])1.7*
delay(duration,[queueName])
设置
jQuery.fx.off
jQuery.fx.interval


$(el).fadeIn();

function fadeIn(el) {
  var opacity = 0;

  el.style.opacity = 0;
  el.style.filter = '';

  var last = +new Date();
  var tick = function() {
    opacity += (new Date() - last) / 400;
    el.style.opacity = opacity;
    el.style.filter = 'alpha(opacity=' + (100 * opacity)|0 + ')';

    last = +new Date();

    if (opacity < 1) {
      (window.requestAnimationFrame && requestAnimationFrame(tick)) || setTimeout(tick, 16);
    }
  };

  tick();
}

fadeIn(el);

//  IE 10+

el.classList.add('show');
el.classList.remove('hide');
.show {
  transition: opacity 400ms;
}
.hide {
  opacity: 0;
}


$(el).hide();

$(el).show();

el.style.display = 'none';

el.style.display = '';
