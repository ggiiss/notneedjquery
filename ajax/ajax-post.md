## ajax 

```javascript
// POST
$.ajax({
  type: 'POST',
  url: '/my/url',
  data: data
});


// POST
var request = new XMLHttpRequest();
request.open('POST', '/my/url', true);
request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
request.send(data);

```
