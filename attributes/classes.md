## CSS 操作

```js

$(el).addClass(className);
// IE 10+
el.classList.add(className);

$(el).removeClass(className);
// IE 10+
el.classList.remove(className);

$(el).toggleClass(className);
// IE 10+
el.classList.toggle(className);
```

### 参考文章

1. http://sentsin.com/web/190.html
2. http://my.oschina.net/hmj/blog/178397

### 兼容性

http://caniuse.com/#feat=classlist

### W3C规范

http://www.w3.org/TR/dom/#domtokenlist
